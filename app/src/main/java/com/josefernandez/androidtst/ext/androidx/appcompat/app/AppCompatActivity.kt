package com.josefernandez.androidtst.ext.androidx.appcompat.app

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/**
 * Wraps a [Toast] message into a simplier function
 */
fun AppCompatActivity.showMessage(message: String, duration: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, message, duration).show()