package com.josefernandez.androidtst

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.josefernandez.androidtst.databinding.ActivitySumBinding
import kotlinx.android.synthetic.main.activity_sum.*
import java.text.DecimalFormat

/**
 * Display UI and manage logic related to the sum numbers test
 */
class SumActivity : AppCompatActivity(), View.OnClickListener, View.OnFocusChangeListener,
    TextView.OnEditorActionListener {
    private val numberFormatter by lazy {
        DecimalFormat().apply { isDecimalSeparatorAlwaysShown = false }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivitySumBinding>(this, R.layout.activity_sum).also {
            it.listener = this@SumActivity
        }

        firstNumber.onFocusChangeListener = this
        secondNumber.apply {
            onFocusChangeListener = this@SumActivity
            setOnEditorActionListener(this@SumActivity)
        }
    }

    override fun onClick(view: View?) {
        sumNumbers()
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        result?.text = ""
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
            sumNumbers()
            true
        } else false
    }

    /**
     * Validate values entered on [firstNumber] and [secondNumber], if valid then display the result
     * in on [result]
     */
    private fun sumNumbers() {
        val firstNumber = parseNumber(firstNumber)
        val secondNumber = parseNumber(secondNumber)
        if (firstNumber == null || secondNumber == null) return

        val resultText = (firstNumber + secondNumber).let { numberFormatter.format(it) }
        result.text = resultText
    }

    /**
     * Validates number value contained on [editText], display an error message if invalid
     *
     * @param editText [EditText] which value will be validated
     * @return [Double] value if correctly converted, null in other case
     */
    private fun parseNumber(editText: EditText): Double? {
        val textNumber = editText.text.toString()
        val number = textNumber.toDoubleOrNull()
        if (number == null) {
            editText.error = getString(R.string.invalid_number_error_message)
        }

        return number
    }
}
